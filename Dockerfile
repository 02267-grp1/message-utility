FROM eclipse-temurin:11-jre-alpine
COPY target/messaging-utilities-3.4.jar /usr/src/
WORKDIR /usr/src/
CMD java -Xmx64m -jar messaging-utilities-3.4.jar
